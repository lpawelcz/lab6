package pc;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class okno extends JFrame{
	
	private JPanel contentPane;
	private kartka kartka;
	private prostokat fabr;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try 
				{
					
					final okno okno = new okno();
					okno.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public okno()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 500, wh = 500;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		kartka = new kartka();// jeszcze powinna byc, zdaje sie jedna klasa nadrzedna, abstrakcyjna poniewaz stworzenie tego obiektu powoduje wyswietlanie sie niechcianego prostokata
		
		contentPane.add(kartka);
		
		
		Dimension size = getBounds().getSize();
		
		JButton btnAdd = new JButton("dodaj prostokat");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Graphics g = null;
				kartka.paintComponent(g);
			}
		});
		btnAdd.setBounds(10, size.height-80, 180, 23);
		contentPane.add(btnAdd);	
	
	
	}

}
